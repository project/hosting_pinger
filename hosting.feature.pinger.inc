<?php

/**
 * Implements hook_hosting_feature().
 */
function hosting_remote_import_hosting_feature() {
  $features['pinger'] = array(
    'title' => t('Pinger'),
    'description' => t('Ping hosted sites regularly to confirm that they are online and responding to requests.'),
    'status' => HOSTING_FEATURE_DISABLED,
    'module' => 'hosting_pinger',
    'group' => 'experimental',
  );
  return $features;
}
